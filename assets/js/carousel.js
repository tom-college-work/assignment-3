var slideIndex = 0; //Store current slide
var slides = document.getElementsByClassName("slides"); //Get all slides

function showSlides() {
    for (i = 0; i < slides.length; i++) {
       slides[i].style.display = "none";  
    }

    slideIndex++;

    if (slideIndex > slides.length - 1) {slideIndex = 0}  

    slides[slideIndex].style.display = "block";  

    setTimeout(showSlides, 6000); // Change image every 4 seconds
}

showSlides(); //Initiate